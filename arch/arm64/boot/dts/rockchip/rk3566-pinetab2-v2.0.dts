// SPDX-License-Identifier: (GPL-2.0+ OR MIT)

/dts-v1/;

#include "rk3566-pinetab2.dtsi"

/ {
	model = "Pine64 PineTab2 v2.0";
	compatible = "pine64,pinetab2-v2.0", "pine64,pinetab2", "rockchip,rk3566";

	// todo, use this + simple pwrseq instead of custom pwrseq driver
	wl_reg: wl-regulator {
		compatible = "regulator-fixed";
//		enable-active-high;
//		gpio = <&gpio0 RK_PA0 GPIO_ACTIVE_HIGH>;
//		pinctrl-names = "default";
//		pinctrl-0 = <&wifi_pwren>;
		regulator-name = "wl_reg";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
		vin-supply = <&vcc_sys>;
//		startup-delay-us = <100000>;
//		off-on-delay-us = <100000>;
	};

	wl_pwrseq: wl-pwrseq {
		compatible = "mmc-pwrseq-bes";
		clocks = <&rk817 1>;
		clock-names = "ext_clock";
		pinctrl-names = "default";
		pinctrl-0 = <&wifi_pwrkey &wifi_reset &wifi_pwren>;
		pwrkey-gpios = <&gpio3 RK_PD3 GPIO_ACTIVE_HIGH>;
		pwren-gpios = <&gpio0 RK_PA0 GPIO_ACTIVE_HIGH>;
		reset-gpios = <&gpio3 RK_PD2 GPIO_ACTIVE_HIGH>;
	};
};

&gpio_keys {
	pinctrl-0 = <&kb_id_det>, <&hall_int_l>;

	event-hall-sensor {
		debounce-interval = <20>;
		gpios = <&gpio0 RK_PA6 GPIO_ACTIVE_LOW>;
		label = "Hall Sensor";
		linux,code = <SW_LID>;
		linux,input-type = <EV_SW>;
		wakeup-event-action = <EV_ACT_DEASSERTED>;
		wakeup-source;
	};
};

&lcd {
	reset-gpios = <&gpio0 RK_PC6 GPIO_ACTIVE_LOW>;
	pinctrl-names = "default";
	pinctrl-0 = <&lcd_pwren_h &lcd0_rst_l>;
};

&pinctrl {
	lcd0 {
		lcd0_rst_l: lcd0-rst-l {
			rockchip,pins = <0 RK_PC6 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};

	hall {
		hall_int_l: hall-int-l {
			rockchip,pins = <0 RK_PA6 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};

	wifi {
		wifi_pwrkey: wifi-pwrkey {
			rockchip,pins = <3 RK_PD3 RK_FUNC_GPIO &pcfg_pull_none>;
		};

		wifi_reset: wifi-reset {
			rockchip,pins = <3 RK_PD2 RK_FUNC_GPIO &pcfg_pull_none>;
		};

		wifi_regon: wifi-regon {
			rockchip,pins = <0 RK_PC0 RK_FUNC_GPIO &pcfg_pull_none>;
		};

		wifi_pwren: wifi-pwren {
			rockchip,pins = <0 RK_PA0 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};
};

&sdmmc1 {
	vmmc-supply = <&wl_reg>;
	mmc-pwrseq = <&wl_pwrseq>;
	#address-cells = <1>;
	#size-cells = <0>;

	bes2600: wifi@1 {
		compatible = "bestechnic,bes2600";
		reg = <1>;
		pinctrl-names = "default";
		pinctrl-0 = <&host_wake_wl &wifi_wake_host_h &wifi_regon>;

		interrupt-parent = <&gpio0>;
		interrupts = <RK_PC4 IRQ_TYPE_EDGE_RISING>;

		device-wakeup-gpios = <&gpio0 RK_PB7 GPIO_ACTIVE_HIGH>; // host_wake_wl
		//host-wakeup-gpios = <&gpio0 RK_PC4 GPIO_ACTIVE_HIGH>; // wifi_wake_host_h
		regon-gpios = <&gpio0 RK_PC0 GPIO_ACTIVE_HIGH>;
	};
};

// for wifi chip comm
&uart1 {
	pinctrl-0 = <&uart1m0_xfer>;
	pinctrl-names = "default";
	status = "okay";
};

&uart1m0_xfer {
	rockchip,pins =
		<2 RK_PB3 2 &pcfg_pull_down>, // RX
		<2 RK_PB4 RK_FUNC_GPIO &pcfg_pull_down>; // TX
};
